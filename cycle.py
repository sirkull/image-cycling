#!/usr/bin/python3

import gi
gi.require_version('Gtk', '4.0')
from gi.repository import Gtk, GdkPixbuf, GLib
import os
import random
from time import sleep
import threading
import argparse
from datetime import datetime

# Setup argparse to accept command line arguments
parser = argparse.ArgumentParser(description="Display images from a folder with GTK")
parser.add_argument("--path", type=str, required=True, help="Path to the image folder")
parser.add_argument("--speed", type=float, default=0.5, help="Speed of image change in seconds")
parser.add_argument("--start", type=str, help="Start date (inclusive) in YYYY-MM-DD format")
parser.add_argument("--end", type=str, help="End date (inclusive) in YYYY-MM-DD format")
parser.add_argument("--nocache", action="store_true", help="Disable caching and load images from disk each time")

args = parser.parse_args()

def filter_images_by_date(image_paths, start_date, end_date):
    if start_date:
        start_date = datetime.strptime(start_date, "%Y-%m-%d")
    if end_date:
        end_date = datetime.strptime(end_date, "%Y-%m-%d")
    filtered_paths = []
    for path in image_paths:
        filename = os.path.basename(path)
        try:
            file_date = datetime.strptime(filename[:10], "%Y-%m-%d")
            if (not start_date or file_date >= start_date) and (not end_date or file_date <= end_date):
                filtered_paths.append(path)
        except ValueError:
            continue  # Skip files that don't match the date format
    return filtered_paths

class ImageWindow(Gtk.ApplicationWindow):
    def __init__(self, app, original_image_paths, speed, use_cache=True):
        super().__init__(title="Image Cycling", application=app)
        self.set_default_size(600, 400)
        self.speed = speed
        self.use_cache = use_cache

        if self.use_cache:
            self.image_cache = self.cache_images(original_image_paths)  # Cache images if enabled
        else:
            self.original_image_paths = original_image_paths  # Store paths for non-cached loading

        self.image_widget = Gtk.Image.new()
        self.set_child(self.image_widget)

        thread = threading.Thread(target=self.display_images_loop, daemon=True)
        thread.start()

    def cache_images(self, image_paths):
        """Preload and cache images."""
        cache = {}
        for path in image_paths:
            try:
                pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(path, 600, 400, preserve_aspect_ratio=True)
                cache[path] = pixbuf
            except Exception as e:
                print(f"Failed to load {path}: {e}")
        return cache

    def display_images_loop(self):
        while True:
            if self.use_cache:
                items = list(self.image_cache.items())
                for _, pixbuf in random.sample(items, len(items)):
                    GLib.idle_add(self.image_widget.set_from_pixbuf, pixbuf)
                    sleep(self.speed)
            else:
                for image_path in random.sample(self.original_image_paths, len(self.original_image_paths)):
                    pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(image_path, 600, 400, preserve_aspect_ratio=True)
                    GLib.idle_add(self.image_widget.set_from_pixbuf, pixbuf)
                    sleep(self.speed)

class ImageApp(Gtk.Application):
    def __init__(self, image_folder, speed, start_date, end_date, use_cache):
        super().__init__()
        self.image_folder = image_folder
        self.speed = speed
        self.start_date = start_date
        self.end_date = end_date
        self.use_cache = use_cache

    def do_activate(self):
        image_paths = [os.path.join(self.image_folder, f) for f in os.listdir(self.image_folder) if f.endswith(('.png', '.jpg', '.jpeg', '.gif'))]
        image_paths = filter_images_by_date(image_paths, self.start_date, self.end_date)
        if not image_paths:
            print("No images found within the specified criteria.")
            return
        win = ImageWindow(self, image_paths, self.speed, not self.use_cache)  # Note the negation to convert --nocache to use_cache
        win.show()

def main():
    app = ImageApp(args.path, args.speed, args.start, args.end, not args.nocache)  # Invert --nocache here for clarity
    app.run()

if __name__ == "__main__":
    main()
