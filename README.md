# image-cycling

## Dependencies

### Debian/Ubuntu

Requires Python3 and GTK4 bindings.

```sudo apt-get install python3 python3-gi python3-gi-cairo gir1.2-gtk-4.0```

### Fedora/RHEL/CentOS

```sudo dnf install python3 python3-gobject gtk4```

## Usage

The script requires a path to be specified with `--path` .

The default cycling time is 0.5 seconds but this can be adjusted with the `--speed` argument.

The `--start` and `--end` dates assume the images are dated (ie `2024-01-01.jpg`, `2024-01-02.jpg`).

By default the images are cached to memory, but that can be disabled with `--nocache`